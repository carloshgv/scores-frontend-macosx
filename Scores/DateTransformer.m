//
//  DateTransformer.m
//  Scores
//
//  Created by Carlos Hugo González Vázquez on 22/06/15.
//  Copyright (c) 2015 Cumulonimbus. All rights reserved.
//

#import "DateTransformer.h"

@implementation DateTransformer

+ (Class)transformedValueClass
{
    return [NSString class];
}

+ (BOOL)allowsReverseTransformation
{
    return NO;
}

- (id)transformedValue:(id)value {
	return [NSDateFormatter localizedStringFromDate:(NSDate *)value dateStyle:NSDateFormatterFullStyle timeStyle:NSDateFormatterNoStyle];
}

@end
