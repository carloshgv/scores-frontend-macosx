//
//  main.m
//  Scores
//
//  Created by Carlos Hugo González Vázquez on 07/06/15.
//  Copyright (c) 2015 Cumulonimbus. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
	return NSApplicationMain(argc, argv);
}
