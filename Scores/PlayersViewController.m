//
//  PlayersViewController.m
//  Scores
//
//  Created by Carlos Hugo González Vázquez on 28/06/15.
//  Copyright (c) 2015 Cumulonimbus. All rights reserved.
//

#import "PlayersViewController.h"

@interface PlayersViewController ()

@end

@implementation PlayersViewController

- (PlayersViewController *)init {
    self = [super init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(update) name:@"update" object:nil];
    return self;
}

- (void)update {
    self.players = [self.service listPlayers];
}

-(void)tableView:(NSTableView *)tableView sortDescriptorsDidChange:(NSArray *)oldDescriptors
{
    [self.players sortUsingDescriptors: [tableView sortDescriptors]];
    [tableView reloadData];
}

-(void)tableViewSelectionDidChange:(NSNotification *)notification {
	NSString* player = ((Player*)[self.playersArrayController selectedObjects][0]).name;
	NSLog(@"%@", player);
	
	self.wins = [self.service winsOf:player];
	self.loses = [self.service losesOf:player];
}

@end
