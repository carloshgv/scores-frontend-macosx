//
//  DisabledScrollView.m
//  Scores
//
//  Created by Carlos Hugo González Vázquez on 13/06/15.
//  Copyright (c) 2015 Cumulonimbus. All rights reserved.
//

#import "DisabledScrollView.h"

@implementation DisabledScrollView

- (void)scrollWheel:(NSEvent *)theEvent {
	[[self nextResponder] scrollWheel:theEvent];
}

@end
