//
//  BackendConnector.m
//  Scores
//
//  Created by Carlos Hugo González Vázquez on 08/06/15.
//  Copyright (c) 2015 Cumulonimbus. All rights reserved.
//

#import "PipeConnector.h"

@import Foundation;

@implementation PipeConnector

- (void)open {
	task = [[NSTask alloc] init];
	NSPipe *inputPipe = [[NSPipe alloc] init];
	NSPipe *outputPipe = [[NSPipe alloc] init];
	
	NSBundle *bundle = [NSBundle mainBundle];
	NSString *connectorPath = [NSString stringWithFormat:@"%@/%@", bundle.bundlePath, self.path];
	NSLog(@"trying to connecto to %@", connectorPath);

	task.launchPath = connectorPath;
	task.arguments = @[ @"--" ];
	task.standardInput = inputPipe;
	task.standardOutput = outputPipe;
	
	@try {
		output = fdopen(inputPipe.fileHandleForWriting.fileDescriptor, "w");
		input = fdopen(outputPipe.fileHandleForReading.fileDescriptor, "r");
		[task launch];
		NSLog(@"connection succesfully opened");
	}
	@catch (NSException *exception) {
		NSLog(@"couldn't start backend in %@", self.path);
	}
}

- (NSString *)sendCommand:(NSString *)command {
	fputs([command cStringUsingEncoding:NSUTF8StringEncoding], output);
	fputc('\n', output);
	fflush(output);
	char line[4096];
	fgets(line, 4096, input);
	
	NSString *string = [[NSString alloc] initWithCString:line encoding:NSUTF8StringEncoding];
	NSLog(@"command: %@", command);
	NSLog(@"output: %@", string);
	return string;
}

- (void)close {
	if(input != nil) {
		fputs("exit\n", output);
		fflush(output);
		[task waitUntilExit];
		int status = task.terminationStatus;
		fclose(output);
		fclose(input);
		if(status == 0)
			NSLog(@"connection succesfully closed");
		else
			NSLog(@"error closing connection");
	}
}

@end
