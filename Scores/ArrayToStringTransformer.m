//
//  ArrayToStringTransformer.m
//  Scores
//
//  Created by Carlos Hugo González Vázquez on 12/06/15.
//  Copyright (c) 2015 Cumulonimbus. All rights reserved.
//

#import "ArrayToStringTransformer.h"

@implementation ArrayToStringTransformer

+ (Class)transformedValueClass
{
    return [NSString class];
}

+ (BOOL)allowsReverseTransformation
{
    return YES;
}

- (id)transformedValue:(id)value {
	return [(NSArray *)value componentsJoinedByString:@", "];
}

- (id)reverseTransformedValue:(id)value {
	NSMutableArray *names = [[NSMutableArray alloc] initWithArray:[(NSString *)value componentsSeparatedByString:@","]];
	for(int i = 0; i < [names count]; i++)
		names[i] = [names[i] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	
	return names;
}

@end
