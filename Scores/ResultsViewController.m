//
//  ResultsView.m
//  Scores
//
//  Created by Carlos Hugo González Vázquez on 07/06/15.
//  Copyright (c) 2015 Cumulonimbus. All rights reserved.
//

#import "ResultsViewController.h"
#import "AppDelegate.h"
#import "PersistentService.h"
#import "Model.h"

@implementation ResultsViewController

- (ResultsViewController *)init {
	self = [super init];
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(update) name:@"update" object:nil];
	return self;
}

- (void)update {
	self.results = [self.service list];
}

@end
