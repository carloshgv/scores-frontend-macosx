//
//  ResultsView.h
//  Scores
//
//  Created by Carlos Hugo González Vázquez on 07/06/15.
//  Copyright (c) 2015 Cumulonimbus. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Protocols.h"

@import AppKit;

@interface ResultsViewController : NSViewController

@property IBOutlet id<ServiceProtocol> service;
@property NSMutableArray* results;

- (ResultsViewController *)init;
- (void)update;

@end
