//
//  PlayersViewController.h
//  Scores
//
//  Created by Carlos Hugo González Vázquez on 28/06/15.
//  Copyright (c) 2015 Cumulonimbus. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "Protocols.h"

@interface PlayersViewController : NSViewController <NSTableViewDataSource, NSTableViewDelegate>

@property IBOutlet id<ServiceProtocol> service;
@property NSMutableArray* players;
@property NSMutableArray* wins;
@property NSMutableArray* loses;
@property IBOutlet NSArrayController *playersArrayController;

- (PlayersViewController *)init;
- (void)update;

@end
