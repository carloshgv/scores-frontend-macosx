//
//  AppDelegate.m
//  Scores
//
//  Created by Carlos Hugo González Vázquez on 07/06/15.
//  Copyright (c) 2015 Cumulonimbus. All rights reserved.
//

#import "AppDelegate.h"
#import "ArrayToStringTransformer.h"

@interface AppDelegate ()

@property (weak) IBOutlet NSWindow *window;

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
	NSLog(@"application finished launching");
	[self.connector open];
	
	ArrayToStringTransformer *transformer = [[ArrayToStringTransformer alloc] init];
	[NSValueTransformer setValueTransformer:transformer forName:@"ArrayToStringTransformer"];
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
	[self.connector close];
}

- (NSApplicationTerminateReply)applicationShouldTerminate:(NSNotification *)aNotification {
	return YES;
}

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSNotification *)aNotification {
	return YES;
}

@end
