//
//  GamesViewController.m
//  Scores
//
//  Created by Carlos Hugo González Vázquez on 26/07/15.
//  Copyright (c) 2015 Cumulonimbus. All rights reserved.
//

#import "GamesViewController.h"

@interface GamesViewController ()

@end

@implementation GamesViewController

- (GamesViewController *)init {
    self = [super init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(update) name:@"update" object:nil];
    return self;
}

- (void)update {
    self.games = [self.service listGames];
}

-(void)tableView:(NSTableView *)tableView sortDescriptorsDidChange:(NSArray *)oldDescriptors
{
    [self.games sortUsingDescriptors: [tableView sortDescriptors]];
    [tableView reloadData];
}

-(void)tableViewSelectionDidChange:(NSNotification *)notification {
	NSString* game = ((NSString*)[self.gamesArrayController selectedObjects][0]);
	NSLog(@"%@", game);
	
	self.winners = [self.service winnersOf:game];
	self.losers = [self.service losersOf:game];
}

@end