//
//  DisabledScrollView.h
//  Scores
//
//  Created by Carlos Hugo González Vázquez on 13/06/15.
//  Copyright (c) 2015 Cumulonimbus. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface DisabledScrollView : NSScrollView

@end
