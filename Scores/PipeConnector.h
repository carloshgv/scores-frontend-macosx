//
//  BackendConnector.h
//  Scores
//
//  Created by Carlos Hugo González Vázquez on 08/06/15.
//  Copyright (c) 2015 Cumulonimbus. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "stdio.h"

#import "Protocols.h"

@import Foundation;

@interface PipeConnector : NSObject <BackendConnectorProtocol>
{
	NSTask *task;
	FILE *output, *input;
}

@property IBOutlet NSString *path;

- (void)open;
- (void)close;
- (NSString *)sendCommand: (NSString *)command;

@end
