//
//  MainWindowController.m
//  Scores
//
//  Created by Carlos Hugo González Vázquez on 07/06/15.
//  Copyright (c) 2015 Cumulonimbus. All rights reserved.
//

#import "MainWindowController.h"
#import "AppDelegate.h"
#import "PersistentService.h"

@interface MainWindowController ()

@end

@implementation MainWindowController

- (void)windowDidLoad {
    [super windowDidLoad];
}

- (void)awakeFromNib {
	[super awakeFromNib];
	
	views = @{
		@"results" : self.resultsView,
		@"players" : self.playersView,
		@"games" : self.gamesView
		//@"calendar" : self.calendarView
	};
	
	[self.toolbar setSelectedItemIdentifier: @"results"];
	[self.window setContentView: self.resultsView.view];
	application = (AppDelegate *)[NSApp delegate];

	dirty = FALSE;
}

- (IBAction)changeCurrentTab: (id)sender {
	[self.window setContentView: ((NSViewController*)views[[(NSToolbarItem *)sender itemIdentifier]]).view];
}

- (IBAction)open: (id)sender {
	NSOpenPanel *panel = [NSOpenPanel openPanel];
	
	panel.canChooseFiles = YES;
	if([panel runModal] == NSOKButton) {
		filename = [panel.URL path];
		[self.service loadFile:filename];
		
		[[NSNotificationCenter defaultCenter] postNotificationName:@"update" object:nil];
	}
}

- (IBAction)save: (id)sender {
	if(filename == nil) {
		NSSavePanel *panel = [NSSavePanel savePanel];
	
		if([panel runModal] == NSOKButton) {
			filename = [panel.URL path];
			[self.service saveFile:filename];
		}
	}
	[self.service saveFile:filename];
	dirty = FALSE;
}

- (IBAction)showAddNewResult: (id)sender {
    self.buffer = [[Result alloc] init];
 
	[NSApp beginSheet:self.panelForNewResult
       modalForWindow:(NSWindow *)self.window
        modalDelegate:self
       didEndSelector:nil
          contextInfo:nil];
}

- (IBAction)cancelNewResult: (id)sender {
	[NSApp endSheet:self.panelForNewResult];
	[self.panelForNewResult orderOut:sender];
}

- (IBAction)addNewResult: (id)sender {
	self.error.hidden = TRUE;
	if([self.service addResult:self.buffer]) {
		[[NSNotificationCenter defaultCenter] postNotificationName:@"update" object:nil];
	
		[NSApp endSheet:self.panelForNewResult];
		[self.panelForNewResult orderOut:sender];
		
		dirty = TRUE;
	} else {
		self.error.hidden = FALSE;
	}
}

- (BOOL)validateToolbarItem:(NSToolbarItem *)toolbarItem
{
    if([toolbarItem.label isEqualToString:@"Gardar"])
		return dirty;
	else
		return toolbarItem.action != nil;
}

@end
