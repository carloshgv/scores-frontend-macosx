//
//  DictionaryService.m
//  Scores
//
//  Created by Carlos Hugo González Vázquez on 08/06/15.
//  Copyright (c) 2015 Cumulonimbus. All rights reserved.
//
#import "PersistentService.h"
#import "Model.h"

@implementation PersistentService

- (void)loadFile: (NSString *)file {
	NSData *output = [[self.connector sendCommand:[NSString stringWithFormat:@"load %@", file]] dataUsingEncoding:NSUTF8StringEncoding];
	NSString *status = [NSJSONSerialization JSONObjectWithData:output options:0 error:nil][@"status"];
	if(![status isEqualToString:@"ok"])
		[NSException raise:@"file load error" format:@"error loading file"];
}

- (void)saveFile: (NSString *)file {
	[self.connector sendCommand:[NSString stringWithFormat:@"save %@", file]];
}

- (NSMutableArray *)list {
	NSData *output = [[self.connector sendCommand:@"list"] dataUsingEncoding:NSUTF8StringEncoding];
	NSArray *dictionaries = [NSJSONSerialization JSONObjectWithData:output options:0 error:nil][@"data"];
	
	NSMutableArray *array = [[NSMutableArray alloc] init];
	for(NSDictionary *dict in dictionaries)
		[array addObject:[self createResultWith:dict]];
	
	return array;
}

- (NSMutableArray *)listPlayers {
    NSData *output = [[self.connector sendCommand:@"list players"] dataUsingEncoding:NSUTF8StringEncoding];
    NSArray *dictionaries = [NSJSONSerialization JSONObjectWithData:output options:0 error:nil][@"data"];
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    for(NSDictionary *dict in dictionaries)
        [array addObject:[self createPlayerWith:dict]];
    
    return array;
}

- (NSMutableArray *)listGames {
    NSData *output = [[self.connector sendCommand:@"list games"] dataUsingEncoding:NSUTF8StringEncoding];
    NSArray *data = [NSJSONSerialization JSONObjectWithData:output options:0 error:nil][@"data"];
	
	NSMutableArray *array = [[NSMutableArray alloc] init];
	for(NSString *string in data)
		[array addObject:string];
	
    return array;
}

- (NSMutableArray *)winsOf:(NSString *)player {
	NSData *output = [[self.connector sendCommand:[NSString stringWithFormat:@"wins %@", player]] dataUsingEncoding:NSUTF8StringEncoding];
	NSArray *dictionaries = [NSJSONSerialization JSONObjectWithData:output options:0 error:nil][@"data"];
	
	NSMutableArray *array = [[NSMutableArray alloc] init];
	for(NSDictionary *dict in dictionaries)
		[array addObject:[self createGameCountWith:dict]];
		
	return array;
}

- (NSMutableArray *)losesOf:(NSString *)player {
	NSData *output = [[self.connector sendCommand:[NSString stringWithFormat:@"loses %@", player]] dataUsingEncoding:NSUTF8StringEncoding];
	NSArray *dictionaries = [NSJSONSerialization JSONObjectWithData:output options:0 error:nil][@"data"];
	
	NSMutableArray *array = [[NSMutableArray alloc] init];
	for(NSDictionary *dict in dictionaries)
		[array addObject:[self createGameCountWith:dict]];
		
	return array;
}

- (NSMutableArray *)winnersOf:(NSString *)game {
	NSData *output = [[self.connector sendCommand:[NSString stringWithFormat:@"winners %@", game]] dataUsingEncoding:NSUTF8StringEncoding];
	NSArray *dictionaries = [NSJSONSerialization JSONObjectWithData:output options:0 error:nil][@"data"];
	
	NSMutableArray *array = [[NSMutableArray alloc] init];
	for(NSDictionary *dict in dictionaries)
		[array addObject:[self createPlayerCountWith:dict]];
		
	return array;
}

- (NSMutableArray *)losersOf:(NSString *)game {
	NSData *output = [[self.connector sendCommand:[NSString stringWithFormat:@"losers %@", game]] dataUsingEncoding:NSUTF8StringEncoding];
	NSArray *dictionaries = [NSJSONSerialization JSONObjectWithData:output options:0 error:nil][@"data"];
	
	NSMutableArray *array = [[NSMutableArray alloc] init];
	for(NSDictionary *dict in dictionaries)
		[array addObject:[self createPlayerCountWith:dict]];
		
	return array;
}

- (BOOL)addResult: (Result*)newResult {
	NSData *data = [[self.connector sendCommand:[[NSString alloc] initWithFormat:@"create %@", [self stringFromResult:newResult]]] dataUsingEncoding:NSUTF8StringEncoding];
	
	NSDictionary *output = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
	return [[output valueForKey:@"status"] isEqualToString:@"ok"];
}

- (Result *)createResultWith: (NSDictionary *)dictionary {
	Result *result = [[Result alloc] init];
	NSDateFormatter *df = [[NSDateFormatter alloc] init];
	df.dateFormat = @"yyyy/MM/dd";

	result.resultId = [(NSString *)[dictionary objectForKey:@"id"] integerValue];
	result.game = (NSString *)[dictionary objectForKey:@"game"];
	result.date = [df dateFromString:(NSString *)[dictionary objectForKey:@"date"]];
	result.scores = [[NSMutableArray alloc] init];
	for(NSDictionary *dict in (NSArray *)[dictionary objectForKey:@"scores"])
		[result.scores addObject:[self createScoreWith:dict]];
	
	return result;
}

- (Player*)createPlayerWith: (NSDictionary *)dictionary {
    Player* player = [[Player alloc] init];
    
    player.name = (NSString *)dictionary[@"player"];
    player.wins = [(NSString *)dictionary[@"wins"] integerValue];
    player.loses = [(NSString *)dictionary[@"loses"] integerValue];
    
    return player;
}

- (Score*)createScoreWith: (NSDictionary *)dictionary {
	Score* score = [[Score alloc] init];
	
	score.points = [(NSString *)[dictionary objectForKey:@"points"] integerValue];
	score.winner = [(NSString *)[dictionary objectForKey:@"winner"] boolValue];
	score.players = [dictionary objectForKey:@"players"];
	
	return score;
}

- (GameCount *)createGameCountWith: (NSDictionary *)dictionary {
	GameCount* count = [[GameCount alloc] init];
	
	count.name = (NSString *)[dictionary objectForKey:@"game"];
	count.count = [(NSString *)[dictionary objectForKey:@"count"] integerValue];

	return count;
}

- (GameCount *)createPlayerCountWith: (NSDictionary *)dictionary {
	GameCount* count = [[GameCount alloc] init];
	
	count.name = (NSString *)[dictionary objectForKey:@"player"];
	count.count = [(NSString *)[dictionary objectForKey:@"count"] integerValue];

	return count;
}

- (NSString*)stringFromResult: (Result*)result {
	NSMutableString *line = [[NSMutableString alloc] init];
	NSDateFormatter *df = [[NSDateFormatter alloc] init];
	df.dateFormat = @"yyyy/MM/dd";
	
	[line appendFormat:@"%@:%@:", [df stringFromDate: result.date], result.game];
	
	NSMutableArray *scoreStrings = [[NSMutableArray alloc] init];
	for(Score *sc in result.scores) {
		[scoreStrings addObject:[NSString stringWithFormat:@"%@(%@)%@",
										  [[sc valueForKey:@"players"] componentsJoinedByString:@","],
										  [sc valueForKey:@"points"],
										  [sc valueForKey:@"winner"] ? @"*" : @""]];
	}
	[line appendString:[scoreStrings componentsJoinedByString:@";"]];
	return line;
}

@end
