//
//  DictionaryService.h
//  Scores
//
//  Created by Carlos Hugo González Vázquez on 08/06/15.
//  Copyright (c) 2015 Cumulonimbus. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Protocols.h"
#import "Model.h"

@interface PersistentService : NSObject <ServiceProtocol>

@property (readwrite) IBOutlet id<BackendConnectorProtocol> connector;

@end
