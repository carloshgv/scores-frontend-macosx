//
//  GamesViewController.h
//  Scores
//
//  Created by Carlos Hugo González Vázquez on 26/07/15.
//  Copyright (c) 2015 Cumulonimbus. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "Protocols.h"

@interface GamesViewController : NSViewController <NSTableViewDataSource, NSTableViewDelegate>

@property IBOutlet id<ServiceProtocol> service;
@property NSMutableArray* games;
@property NSMutableArray* winners;
@property NSMutableArray* losers;
@property IBOutlet NSArrayController *gamesArrayController;

- (GamesViewController *)init;
- (void)update;

@end