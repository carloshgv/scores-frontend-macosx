//
//  AppDelegate.h
//  Scores
//
//  Created by Carlos Hugo González Vázquez on 07/06/15.
//  Copyright (c) 2015 Cumulonimbus. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "PipeConnector.h"

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property IBOutlet id<BackendConnectorProtocol> connector;

@end

