//
//  Model.h
//  Scores
//
//  Created by Carlos Hugo González Vázquez on 15/06/15.
//  Copyright (c) 2015 Cumulonimbus. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Result : NSObject

@property NSInteger resultId;
@property NSString *game;
@property NSDate *date;
@property NSMutableArray *scores;

- (Result*)init;

@end

@interface Score : NSObject

@property NSArray* players;
@property NSInteger points;
@property BOOL winner;

@end

@interface Player : NSObject

@property NSString* name;
@property NSInteger wins;
@property NSInteger loses;

- (Player*)init;

@end

@interface GameCount : NSObject

@property NSString* name;
@property NSInteger count;

@end

@interface PlayerCount : NSObject

@property NSString* name;
@property NSInteger count;

@end
