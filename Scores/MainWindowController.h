//
//  MainWindowController.h
//  Scores
//
//  Created by Carlos Hugo González Vázquez on 07/06/15.
//  Copyright (c) 2015 Cumulonimbus. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "Protocols.h"
#import "Model.h"

@class AppDelegate;

@interface MainWindowController : NSWindowController
{
	NSDictionary *views;
	AppDelegate *application;
	NSString *filename;
	BOOL dirty;
}

@property (strong) IBOutlet NSViewController *resultsView;
@property (strong) IBOutlet NSViewController *playersView;
@property (strong) IBOutlet NSViewController *gamesView;
@property (strong) IBOutlet NSViewController *calendarView;
@property (strong) IBOutlet NSToolbar *toolbar;
@property (strong) IBOutlet NSPanel *panelForNewResult;

@property (strong) IBOutlet NSArrayController *controller;

@property (strong) Result *buffer;

@property (strong) IBOutlet NSTextField *error;
@property (strong) IBOutlet NSTableView *scoresTable;

@property IBOutlet id<ServiceProtocol> service;

- (IBAction)changeCurrentTab: (id)sender;
- (IBAction)open: (id)sender;
- (IBAction)save: (id)sender;
- (IBAction)showAddNewResult: (id)sender;
- (IBAction)cancelNewResult: (id)sender;
- (IBAction)addNewResult: (id)sender;

@end
