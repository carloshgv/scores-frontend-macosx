//
//  Protocols.h
//  Scores
//
//  Created by Carlos Hugo González Vázquez on 08/06/15.
//  Copyright (c) 2015 Cumulonimbus. All rights reserved.
//

#ifndef Scores_Protocols_h
#define Scores_Protocols_h

#import "Model.h"

@protocol BackendConnectorProtocol

- (void)open;
- (void)close;
- (NSString *)sendCommand: (NSString *)command;

@end

@protocol ServiceProtocol

- (void)loadFile: (NSString *)file;
- (void)saveFile: (NSString *)file;
- (NSMutableArray *)list;
- (NSMutableArray *)listPlayers;
- (NSMutableArray *)listGames;
- (NSMutableArray *)winsOf:(NSString *)player;
- (NSMutableArray *)losesOf:(NSString *)player;
- (NSMutableArray *)winnersOf:(NSString *)game;
- (NSMutableArray *)losersOf:(NSString *)game;
- (BOOL)addResult: (Result*)newResult;

@end

#endif
