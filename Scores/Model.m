//
//  Model.m
//  Scores
//
//  Created by Carlos Hugo González Vázquez on 15/06/15.
//  Copyright (c) 2015 Cumulonimbus. All rights reserved.
//

#import "Model.h"

@implementation Result

- (Result*)init {
	self.game = [[NSString alloc] init];
	self.date = [[NSDate alloc] init];
	self.scores = [[NSMutableArray alloc] init];
	
	return self;
}

@end

@implementation Score

@end

@implementation Player

- (Player*)init {
    self.name = [[NSString alloc] init];
    self.wins = 0;
    self.loses = 0;
    
    return self;
}

@end

@implementation GameCount

- (GameCount*)init {
	self.name = [[NSString alloc] init];
	self.count = 0;
	
	return self;
}

@end

@implementation PlayerCount

- (PlayerCount*)init {
	self.name = [[NSString alloc] init];
	self.count = 0;
	
	return self;
}

@end
